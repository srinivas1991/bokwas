package com.start;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.neo4j.shell.util.json.JSONArray;
import org.neo4j.shell.util.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Servlet implementation class StoreFriends
 */
public class StoreFriends extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StoreFriends() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String fbdata="";
	  	
		
		BufferedReader in = new BufferedReader(new InputStreamReader(
                request.getInputStream()));
String line = in.readLine();
String fbid=request.getParameter("fbid");
String fbname=request.getParameter("fbname");


while (line != null) {
    fbdata += line;
    line = in.readLine();
}
try{
JSONObject jobj=new JSONObject(fbdata);
		   JSONArray feedArr = jobj.getJSONArray("data");
		  
		   CreateSimpleGraph db=new CreateSimpleGraph();   
		   db.checkDatabaseIsRunning();
	          	 StringBuilder b=new StringBuilder();
              	 b.append("[");
              	 int k=0;
              	 URI frmuri=new URI("http://localhost:7474/db/data/index/node/fbid?uniqueness=get_or_create");
              	 String s="{\"key\":\"id\",\"value\":\""+fbid+"\",\"properties\":{\"name\":\""+fbname+"\"}}";
                 WebResource resource = Client.create()
                         .resource(frmuri);
                 ClientResponse res= resource.accept( MediaType.APPLICATION_JSON )
                         .type( MediaType.APPLICATION_JSON )
                         .entity(s)
                         .post( ClientResponse.class );
              URI me=res.getLocation();
              res.close();

              	 for (int i = 0; i < feedArr.length(); i++) {

            	   JSONObject item = feedArr.getJSONObject(i);
                //friend=db.createNode();/people?uniqueness=get_or_create//MyIndex/?uniqueness=get_or_create
                	b.append("{\"method\":\"POST\",\"to\":\"/index/node/fbid?uniqueness=get_or_create\",\"id\":"+k+",\"body\":{\"key\":\"id\",\"value\":\""+ item.getString("id")+"\",\"properties\":{\"name\":\""+item.getString("name")+"\"}}},");
            	   k++;
               //  b.append("{\"method\":\"PUT\",\"to\":\"{"+(k-1)+"}/properties\",\"id\":"+k+",\"body\":{\"name\":\""+ item.getJSONObject("from").getString("name")+"\"}},");
                //k++;
                 b.append("{\"method\":\"POST\",\"to\":\"/index/relationship/uniquedge?uniqueness=get_or_create\",\"id\":"+k+",\"body\":{\"key\":\"rel_id\",\"value\":\""+item.getString("id")+"_"+fbid+"\",\"start\":\""+me+"\",\"end\":\"{"+(k-1)+"}\",\"type\":\"is a connection\"}},");
               k++;           
               }
               k=0;
               b.deleteCharAt(b.length()-1);
               b.append("]");
               
              // JSONObject j=new JSONObject(b.toString());
              URI fromuri=new URI("http://localhost:7474/db/data/batch");
               //System.out.println(j.toString());
              WebResource  resrc = Client.create()
                       .resource(fromuri);
               // POST JSON to the relationships URI
               ClientResponse resp= resrc.accept( MediaType.APPLICATION_JSON )
                       .type( MediaType.APPLICATION_JSON )
                       .entity(b.toString())
                       .post( ClientResponse.class );
System.out.println(resp.getStatus());

               resp.close();

             // db.findSingersInBands(me); */
}catch(Exception e){
System.out.println(e.getMessage());
e.printStackTrace();
}

	}

}
