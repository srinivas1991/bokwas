package com.start;

import java.io.File;
import java.io.IOException;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.impl.util.FileUtils;

public class EmbeddedNeo4j
{
    private static final String DB_PATH = "target/bokwasdb";

    public String greeting;

    // START SNIPPET: vars
    GraphDatabaseService graphDb;
    Node firstNode;
    Node secondNode;
    Relationship relationship;
    // END SNIPPET: vars

    public static void main(String args[])
    {
    	EmbeddedNeo4j db=new EmbeddedNeo4j();
    	db.createDb();
    	db.removeData();
    	db.shutDown();
    }
    // START SNIPPET: createReltype
    private static enum RelTypes implements RelationshipType
    {
        KNOWS
    }
    // END SNIPPET: createReltype


    void createDb()
    {
       // clearDb();
        // START SNIPPET: startDb
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase( DB_PATH );
        registerShutdownHook( graphDb );
        // END SNIPPET: startDb
        try ( Transaction tx = graphDb.beginTx() )
        {
            // Database operations go here
            // END SNIPPET: transaction
            // START SNIPPET: addData
            //     relationship = .createRelationshipTo(friend, RelTypes.KNOWS );
            //relationship.setProperty( "message", "is a friend of " );
            // END SNIPPET: addData

            // START SNIPPET: readData
            //System.out.print( relationship.getProperty( "message" ) );
            // END SNIPPET: readData

           
            // START SNIPPET: transaction
            tx.success();
        }
        // END SNIPPET: transaction
    }

    private void clearDb()
    {
        try
        {
            FileUtils.deleteRecursively( new File( DB_PATH ) );
        }
        catch ( IOException e )
        {
            throw new RuntimeException( e );
        }
    }

    void removeData()
    {
        try ( Transaction tx = graphDb.beginTx() )
        {
            // START SNIPPET: removingData
            // let's remove the data
            //firstNode.getSingleRelationship( RelTypes.KNOWS, Direction.OUTGOING ).delete();
            //firstNode.delete();
            //secondNode.delete();
            // END SNIPPET: removingData

            tx.success();
        }
    }

    void shutDown()
    {
        System.out.println();
        System.out.println( "Shutting down database ..." );
    try{    // START SNIPPET: shutdownServer
        graphDb.shutdown();
    }
    catch(Exception e)
    {System.out.println(e.getMessage());}// END SNIPPET: shutdownServer
    }

    // START SNIPPET: shutdownHook
    private static void registerShutdownHook( final GraphDatabaseService graphDb )
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook( new Thread()
        {
            @Override
            public void run()
            {
                graphDb.shutdown();
            }
        } );
    }
    // END SNIPPET: shutdownHook
}