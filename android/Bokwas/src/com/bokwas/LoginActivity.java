package com.bokwas;

import java.io.InputStream;
import java.util.Arrays;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener; 
import android.widget.Toast; 
  



import com.bokwas.dialogboxes.GenericDialogOk;
import com.facebook.HttpMethod;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

public class LoginActivity extends Activity implements OnClickListener {
	 
	    public String greeting;

	  
    private String facebook_id="";
	private String TAG = "LoginActivity";
	private UiLifecycleHelper uiHelper;
	private LoginButton authButton;
	private String fbname="";
	SharedPreferences prefs;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		prefs = this.getSharedPreferences(
			      "com.example.app", Context.MODE_PRIVATE);

		uiHelper = new UiLifecycleHelper(this, callback);
	    uiHelper.onCreate(savedInstanceState);
		setContentView(R.layout.login_page);
		authButton = (LoginButton) findViewById(R.id.facebookButton);
		authButton.setBackgroundResource(R.drawable.fb_login_main_button);
		authButton.setText("");
		authButton.setReadPermissions(Arrays.asList("email", "user_about_me","read_stream"));
		setOnClickListeners();

	}

	private void setOnClickListeners() {
		// findViewById(R.id.facebookButton).setOnClickListener(this);
		findViewById(R.id.whyFacebookLoginButton).setOnClickListener(this);
	}
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
	        onSessionStateChange(session, state, exception);
	    }
	};
 
	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		
		if (session.isOpened()) {
			Log.d(TAG,"AccessToken :"+session.getAccessToken());
			Toast.makeText(this, "Logged in...", Toast.LENGTH_SHORT).show();
			authButton.setText("");
			getUserData();
			//getfriends();
			//storefeed();
			moveToNextScreen();
		}

	}

	private void getfriends() {
		Session session = Session.getActiveSession();
		   Request request = new Request(session,
		       "/me/friends",                         
		       null,                        
		       HttpMethod.GET,                 
		       new Request.Callback(){         
		           public void onCompleted(Response response) {
		            HttpClient client = new DefaultHttpClient();
		            HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000); //Timeout Limit
		            HttpResponse res;
		        	SharedPreferences prefs = LoginActivity.this.getSharedPreferences(
						      "com.bokwas.app", Context.MODE_PRIVATE);
				String fbname=prefs.getString("fbname","");
				String facebook_id=prefs.getString("fbid","");
				
		            try { 
		            	JSONObject ja=response.getGraphObject().getInnerJSONObject();
		                 	 
		                HttpPost post = new HttpPost("http://10.0.2.2:8080/Datastore/StoreFriends?fbid="+facebook_id+"&fbname="+fbname);
		                StringEntity se = new StringEntity(ja.toString());  
		                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		                post.setEntity(se);
		                res = client.execute(post);
		                /*Checking response */
		                if(res!=null){
		                    InputStream in = res.getEntity().getContent(); //Get the data in the entity
		                    System.out.println(in.toString());
		                    
		                }
		                storefeed();

		            } catch(Exception e) {
		                e.printStackTrace();
		            }         	   //Log.i(TAG, "Result: " + response.toString());
		           }                  
		   });  
		   Request.executeBatchAsync(request); 
		
	}

	private void moveToNextScreen() {
		Intent intent = new Intent(this, ProfileChooserActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.activity_slide_in_left,
				R.anim.activity_slide_out_left);
		finish();
	} 
 
	public void storefeed()
	{
   Session session = Session.getActiveSession();
   Request request = new Request(session,
       "/me/home",                         
       null,                        
       HttpMethod.GET,                 
       new Request.Callback(){         
           public void onCompleted(Response response) {
        	   if(response.getError()==null)
        	   {
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000); //Timeout Limit
            HttpResponse res;
        	SharedPreferences prefs = LoginActivity.this.getSharedPreferences(
				      "com.bokwas.app", Context.MODE_PRIVATE);
		String fbname=prefs.getString("fbname","");
		String facebook_id=prefs.getString("fbid","");
		Log.d("fbname:",fbname+facebook_id);
	  
            try {  
            	JSONObject ja=response.getGraphObject().getInnerJSONObject();
                 	 
                HttpPost post = new HttpPost("http://10.0.2.2:8080/Datastore/Connect?fbid="+facebook_id+"&fbname="+fbname);
                StringEntity se = new StringEntity(ja.toString());  
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                res = client.execute(post);
                /*Checking response */
                if(res!=null){
                    InputStream in = res.getEntity().getContent(); //Get the data in the entity
                    System.out.println(in.toString());
                    
                } 

            } catch(Exception e) {
                e.printStackTrace();
            }   
        	   }else {
        		   Toast.makeText(LoginActivity.this,response.getError().toString(),Toast.LENGTH_LONG).show();
        		   }
           }                  
   });  
   Request.executeBatchAsync(request); 
	} 
	@SuppressWarnings("deprecation")
	public void getUserData() {
		Request.executeMeRequestAsync(Session.getActiveSession(),
				new GraphUserCallback() {
  
				@Override
					public void onCompleted(GraphUser user, Response response) {
					if(response.getError()==null)
					{
					SharedPreferences prefs = LoginActivity.this.getSharedPreferences(
							"com.bokwas.app", Context.MODE_PRIVATE);
					prefs.edit().putString("fbid",""+user.getId()).commit();
					prefs.edit().putString("fbname",user.getFirstName()+user.getLastName()).commit();
					getfriends();
						Log.d(TAG, "FirstName :"+ user.getFirstName());
						Log.d(TAG, "LastName :"+ user.getLastName());
						Log.d(TAG, "Gender :"+ user.getProperty("gender").toString());
						Log.d(TAG, "Facebook Id :"+ user.getId());
						facebook_id=user.getId();
						fbname=user.getFirstName()+""+user.getLastName();
						fbname=fbname.replaceAll(" ","%20");
						System.out.println(fbname);
					}else{
						Log.d("facebookerror",response.getError().getErrorMessage());
						Toast.makeText(LoginActivity.this,response.getError().toString(),Toast.LENGTH_LONG).show();
		        	}
				}
				}); 
	}  
 
	@Override
	public void onResume() {
	    super.onResume();
	    storefeed();
	    uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.facebookButton) {

		} else if (view.getId() == R.id.whyFacebookLoginButton) {
			GenericDialogOk dialog = new GenericDialogOk(
					this,
					"Why connect to facebook?",
					"We retrieve only posts from facebook so that you can talk about it on Bokwas. We do NOT post anything back on facebook.");
			dialog.show();
		} 
	}

}
